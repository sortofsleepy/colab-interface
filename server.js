const express = require("express");
const minimist = require("minimist")
const {PubSub} = require("@google-cloud/pubsub");

// for reference
//https://github.com/googleapis/nodejs-pubsub

// get server args
let args = minimist(process.argv.slice(2));

// define the port. 3001 by default.
const port = args.port !== undefined ? args.port : 3001;

// if Auth env doesn't exist, add it.
// TODO replace with your own key. See https://cloud.google.com/docs/authentication/getting-started
if(!process.env.hasOwnProperty("GOOGLE_APPLICATION_CREDENTIALS")){
    process.env["GOOGLE_APPLICATION_CREDENTIALS"] = args.auth_key !== undefined ? args.auth_key : "./auth.json";
}

const projectId = args.project_id !== undefined ? args.project_id : "colab-tests"
const bucketName = args.bucket_name !== undefined ? args.bucket_name : "colab-tests.appspot.com";
const topicName = args.topic_name !== undefined ? args.topic_name : "colab-test";


const app = express();
let pubsub = null;
app.listen(port,() => {
    console.log('Server started');
    pubsub = new PubSub({projectId});

    init();
});


/**
 * Initialize and ensure topic exists in storage.
 */
function init(){

    pubsub.getTopics().then(e =>{

        // if no topics exist then create topic based on arg.
        if(e.length === 0) {
            pubsub.createTopic(topicName).catch(e => {
                console.error("unable to create topic: ", e.details);
            });
        }
    })
}

// core path.
app.get("/",(req,res) => {
    res.send("Nothing to see here.")
});

// process information coming from front end.
app.get("/process/:uri",(res,res) => {

});