colab-interface
====

This is a just a proof of concept of setting up a front-end web experience and tie it to 
a Google Colab notebook. 

This is pretty much entirely based on the work of [Cyril Diagne](https://github.com/cyrildiagne)'s work, [instagram-3d-photo](https://github.com/cyrildiagne/instagram-3d-photo)
but re-arranged to be more focused on the interface between Colab and a web front-end rather than 
a specific purpose. Also uses nodejs vs python as it's usually simpler to set up a server on a VPS compared to Python

Setup
=====
* `npm install`
* start the server `node server.js`
* path defaults to `localhost:3001` unless you pass in the `--port` flag

Full flags
===
* `--port` : the port you want to run the server on 
* `--project_id` : the project id on GCP that you've set up a pub/sub service on
* `--bucket_name` : the bucket within your project that you're using for storage
* `--topic_name` : the topic within the bucket that you'e using for your project.
* `--auth_key` : the path to your authentication key assuming you don't have an environmental variable setup.